import { expect } from 'chai';
import { FileConf } from './index.mjs';
import z from 'zod';

const mySchema = z
  .object({
    mode: z.enum(['development', 'production']).default('development'),
    database: z
      .object({
        host: z.string().default('somewhere.net'),
        port: z.coerce.number().default(5432),
        user: z.string().default('REDACTED'),
        password: z.string().default('REDACTED')
      })
      .default({})
  })
  .strict()
  .default({});
/**
 * @typedef {object} Test
 * @property {string} name
 */

class Config extends FileConf {
  constructor(format) {
    super(mySchema, { format });
    /** @type {Test} */
    this.$json;
  }
  get mode() {
    return this.$json.mode;
  }
}

describe('FileConf Test', function () {
  it('Should load a file using ini format', function () {
    const c = new Config('ini');
    expect(c.mode).to.equal('development');
    c.delete();
  });
  it('Should load a file using yaml format', function () {
    const c = new Config('yaml');
    expect(c.mode).to.equal('development');
    c.delete();
  });
  it('Should load a file using json format', function () {
    const c = new Config();
    expect(c.mode).to.equal('development');
    c.delete();
    1;
  });
  it('Should accept valid changes and save them to disk', function () {
    const c = new Config();
    expect(c.mode).to.equal('development');
    expect(c.update({ mode: 'production' }), true).to.be.true;
    expect(c.mode).to.equal('production');
    c.delete();
  });
  it('Should reload saved changes from disk', function () {
    const c = new Config();
    expect(c.mode).to.equal('development');
    expect(c.update({ mode: 'production' }, true)).to.be.true;
    expect(c.mode).to.equal('production');
    const c2 = new Config();
    expect(c2.mode).to.equal('production');
    c2.delete();
  });
  it('Should not reload unsaved changes from disk', function () {
    const c = new Config();
    expect(c.mode).to.equal('development');
    expect(c.update({ mode: 'production' })).to.be.true;
    expect(c.mode).to.equal('production');
    const c2 = new Config();
    expect(c2.mode).to.equal('development');
    c2.delete();
  });
});
