import fs from 'fs';
import path from 'path';
import yaml from 'yaml';
import ini from 'ini';
import z from 'zod';

/**
 * Default mode of printing file
 * @constant
 * @default
 */
const FORMAT = 'json';
/**
 * Default path. The value is null, meaning file-conf will determine the path to be local to the
 * project with the name config.[json,yaml,ini]
 * @constant
 * @default
 */
const PATH = null;

const optionsSchema = z
  .object({
    format: z.enum(['json', 'yaml', 'ini']).default(FORMAT),
    path: z.string().nullable().default(PATH)
  })
  .default({});

/**
 * @typedef {object} FileConfOptions
 * @property {"json"|"yaml"|"ini"} [format] Format the configuration file will be saved in. Default: JSON
 * @property {string} path
 */

/**
 * @callback FileParser
 * @param {string} content
 * @returns {object}
 */

/**
 * @callback FileSerializer
 * @param {object} json
 * @returns {string}
 */

export class FileConf {
  /** @type {FileConfOptions} */
  #options;
  /** @type {FileParser} */
  #parse;
  /** @type {FileSerializer} */
  #serialize;
  /** @type {z.ZodSchema} */
  #schema;
  $json;
  /**
   *
   * @param {z.ZodSchema} schema
   * @param {FileConfOptions} options
   */
  constructor(schema, options) {
    this.#options = optionsSchema.parse(options);
    this.#schema = schema;
    if (this.#options.path === null)
      this.#options.path = path.resolve(`config.${this.#options.format}`);
    switch (this.#options.format) {
      case 'ini':
        this.#parse = ini.parse;
        this.#serialize = ini.stringify;
        break;
      case 'yaml':
        this.#parse = yaml.parse;
        this.#serialize = yaml.stringify;
        break;
      default:
        this.#parse = JSON.parse;
        this.#serialize = (json) => JSON.stringify(json, null, 2);
        break;
    }
    this.$json = this.#schema.parse(this.#loadFile());
    this.save();
  }
  /**
   * Update the configuration by applying an object to the existing object. Returns true/false
   * indicating successful vs failed update.
   * @param {unknown} object
   * @param {boolean} permanent True will save the change to disk, false leaves the change runtime only
   * @returns {boolean}
   */
  update(object, permanent = false) {
    const copy = JSON.parse(JSON.stringify(this.$json));
    const potentialConfig = Object.assign(copy, object);
    const result = this.#schema.safeParse(potentialConfig);
    if (result.success) {
      this.$json = result.data;
      if (permanent) this.save();
    }
    return result.success;
  }
  /**
   * @protected
   * Determines if the current version of the configuration differs from the version stored on disk
   * @returns {boolean}
   */
  #requireSave() {
    return JSON.stringify(this.#loadFile()) !== JSON.stringify(this.$json);
  }
  /**
   * @protected
   * Load the file data from disk
   * @returns {unknown}
   */
  #loadFile() {
    return fs.existsSync(this.#options.path)
      ? this.#parse(fs.readFileSync(this.#options.path).toString())
      : {};
  }
  /**
   * Save the current state of configuration to disk
   */
  save() {
    if (this.#requireSave()) fs.writeFileSync(this.#options.path, this.#serialize(this.$json));
  }
  /**
   * Delete the existing configuration file
   */
  delete() {
    fs.unlinkSync(this.#options.path);
  }
}

export default FileConf;
